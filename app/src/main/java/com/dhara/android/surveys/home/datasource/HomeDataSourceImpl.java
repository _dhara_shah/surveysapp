package com.dhara.android.surveys.home.datasource;

import com.dhara.android.surveys.SurveysApp;
import com.dhara.android.surveys.dagger2.injector.NetInjector;
import com.dhara.android.surveys.network.Listener;
import com.dhara.android.surveys.network.ServiceError;
import com.dhara.android.surveys.network.api.Api;
import com.dhara.android.surveys.network.api.RestApi;
import com.dhara.android.surveys.network.entity.Oauth;
import com.dhara.android.surveys.network.entity.Surveys;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class HomeDataSourceImpl implements HomeDataSource {
    @Inject
    RestApi restApiService;

    public HomeDataSourceImpl() {
        NetInjector.from(SurveysApp.getInstance()).inject(this);
    }

    @Override
    public void fetchSurveys(final String accessToken, int page, final Listener<List<Surveys>> listener) {
        restApiService.getSurveys(accessToken, page)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(error -> listener.onError(new ServiceError(error.getMessage())))
                .onErrorReturn(throwable -> new ArrayList<>())
                .subscribe(listener::onSuccess);
    }
}
