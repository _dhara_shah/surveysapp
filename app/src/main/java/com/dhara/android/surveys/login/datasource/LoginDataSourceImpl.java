package com.dhara.android.surveys.login.datasource;

import com.dhara.android.surveys.SurveysApp;
import com.dhara.android.surveys.dagger2.injector.NetInjector;
import com.dhara.android.surveys.network.Listener;
import com.dhara.android.surveys.network.ServiceError;
import com.dhara.android.surveys.network.api.Api;
import com.dhara.android.surveys.network.api.RestApi;
import com.dhara.android.surveys.network.entity.Oauth;
import com.dhara.android.surveys.utils.SurveysAppLog;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginDataSourceImpl implements LoginDataSource {
    private static final String TAG = LoginDataSourceImpl.class.getSimpleName();

    @Inject
    RestApi restApiService;

    @Inject
    SurveysAppLog logger;

    public LoginDataSourceImpl() {
        NetInjector.from(SurveysApp.getInstance()).inject(this);
    }

    @Override
    public void getAccessToken(final Listener<Oauth> listener) {
        final Call<Oauth> callable = restApiService.getAccessToken(Api.getGrantType(),
                Api.getUserName(), Api.getPassword());
        callable.enqueue(new Callback<Oauth>() {
            @Override
            public void onResponse(final Call<Oauth> call, final Response<Oauth> response) {
                if (response.isSuccessful()) {
                    listener.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(final Call<Oauth> call, final Throwable t) {
                logger.wtf(TAG, t);
                listener.onError(new ServiceError(t.getMessage()));
            }
        });
    }
}
