package com.dhara.android.surveys.login.router;

import android.support.v7.app.AppCompatActivity;

import com.dhara.android.surveys.home.view.HomeActivity;

import javax.inject.Inject;

public class LoginRouterImpl implements LoginRouter {
    private AppCompatActivity activity;

    @Inject
    public LoginRouterImpl(final AppCompatActivity activity) {
        this.activity = activity;
    }

    @Override
    public void close() {
        activity.finish();
    }

    @Override
    public void navigateToHome() {
        HomeActivity.startActivity(activity);
        close();
    }
}
