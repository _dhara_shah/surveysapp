package com.dhara.android.surveys.login.router;

public interface LoginRouter {
    void close();

    void navigateToHome();
}
