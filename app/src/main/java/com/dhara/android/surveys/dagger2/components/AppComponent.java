package com.dhara.android.surveys.dagger2.components;

import android.app.Application;

import com.dhara.android.surveys.SurveysApp;
import com.dhara.android.surveys.dagger2.modules.AppModule;
import com.dhara.android.surveys.utils.SurveysAppLog;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {
    void inject(SurveysApp surveysApp);

    void inject(SurveysAppLog surveysAppLog);

    Application application();

    SurveysAppLog getSurveysLog();
}
