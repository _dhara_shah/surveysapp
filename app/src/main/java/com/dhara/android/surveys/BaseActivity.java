package com.dhara.android.surveys;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.dhara.android.surveys.dagger2.components.ActivityComponent;
import com.dhara.android.surveys.dagger2.components.DaggerActivityComponent;
import com.dhara.android.surveys.dagger2.modules.ActivityModule;

public abstract class BaseActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    public ActivityComponent getActivityComponent() {
        return DaggerActivityComponent.builder()
                .surveysAppComponent(((SurveysApp)getApplication()).getSurveysAppComponent())
                .appComponent(((SurveysApp) getApplication()).getAppComponent())
                .activityModule(new ActivityModule(this))
                .build();
    }
}