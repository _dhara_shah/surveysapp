package com.dhara.android.surveys.network.entity;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Surveys {
    private String id;
    private String title;
    private String description;
    @SerializedName("cover_image_url")
    private String coverImageUrl;
    private String type;
    @SerializedName("short_url")
    private String shortUrl;
    private ArrayList<Question> questions;

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getCoverImageUrl() {
        return coverImageUrl;
    }

    public String getType() {
        return type;
    }

    public String getShortUrl() {
        return shortUrl;
    }

    public String getCoverImageUrlLarge() {
        return coverImageUrl + "l";
    }

    public ArrayList<Question> getQuestions() {
        return questions;
    }
}
