package com.dhara.android.surveys.dagger2.modules;

import com.dhara.android.surveys.home.datasource.HomeDataSource;
import com.dhara.android.surveys.home.datasource.HomeDataSourceImpl;
import com.dhara.android.surveys.home.model.HomeInteractor;
import com.dhara.android.surveys.home.model.HomeInteractorImpl;
import com.dhara.android.surveys.login.datasource.LoginDataSource;
import com.dhara.android.surveys.login.datasource.LoginDataSourceImpl;
import com.dhara.android.surveys.login.model.LoginInteractor;
import com.dhara.android.surveys.login.model.LoginInteractorImpl;
import com.google.gson.Gson;

import dagger.Module;
import dagger.Provides;

@Module
public class SurveysAppModule {
    @Provides
    public Gson provideGson() {
        return new Gson();
    }

    @Provides
    public HomeInteractor provideHomeInteractor() {
        return new HomeInteractorImpl();
    }

    @Provides
    public HomeDataSource provideHomeDataSource() {
        return new HomeDataSourceImpl();
    }

    @Provides
    public LoginInteractor provideLoginInteractor() {
        return new LoginInteractorImpl();
    }

    @Provides
    public LoginDataSource provideLoginDataSource() {
        return new LoginDataSourceImpl();
    }
}
