package com.dhara.android.surveys.home.view;

public interface ViewInteractionListener {
    void onRefreshClicked();

    void onTakeSurveysClicked(int position);
}
