package com.dhara.android.surveys.network;

public interface Listener<T> {
    void onSuccess(T response);

    void onError(ServiceError error);
}
