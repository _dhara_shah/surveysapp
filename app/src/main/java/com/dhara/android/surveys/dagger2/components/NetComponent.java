package com.dhara.android.surveys.dagger2.components;

import com.dhara.android.surveys.dagger2.modules.AppModule;
import com.dhara.android.surveys.dagger2.modules.NetModule;
import com.dhara.android.surveys.home.datasource.HomeDataSourceImpl;
import com.dhara.android.surveys.login.datasource.LoginDataSourceImpl;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class, NetModule.class})
public interface NetComponent {
    void inject(HomeDataSourceImpl homeDataSource);

    void inject(LoginDataSourceImpl loginDataSource);
}
