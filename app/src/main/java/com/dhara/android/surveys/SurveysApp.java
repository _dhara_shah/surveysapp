package com.dhara.android.surveys;

import android.app.Application;
import android.content.Context;

import com.dhara.android.surveys.dagger2.components.AppComponent;
import com.dhara.android.surveys.dagger2.components.DaggerAppComponent;
import com.dhara.android.surveys.dagger2.components.DaggerNetComponent;
import com.dhara.android.surveys.dagger2.components.DaggerSurveysAppComponent;
import com.dhara.android.surveys.dagger2.components.NetComponent;
import com.dhara.android.surveys.dagger2.components.SurveysAppComponent;
import com.dhara.android.surveys.dagger2.injector.AppComponentProvider;
import com.dhara.android.surveys.dagger2.modules.AppModule;
import com.dhara.android.surveys.dagger2.modules.NetModule;
import com.dhara.android.surveys.dagger2.modules.SurveysAppModule;
import com.dhara.android.surveys.network.api.Api;

public class SurveysApp extends Application implements AppComponentProvider {
    private static SurveysApp INSTANCE;
    private AppComponent appComponent;
    private NetComponent netComponent;
    private SurveysAppComponent surveysAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        INSTANCE = this;
        getAppComponent().inject(this);
    }

    public static SurveysApp getInstance() {
        return INSTANCE;
    }

    public AppComponent getAppComponent() {
        if (appComponent == null) {
            appComponent = DaggerAppComponent.builder()
                    .appModule(new AppModule(this))
                    .build();
        }
        return appComponent;
    }

    public SurveysAppComponent getSurveysAppComponent() {
        if (surveysAppComponent == null) {
            surveysAppComponent = DaggerSurveysAppComponent.builder()
                    .appModule(new AppModule(this))
                    .surveysAppModule(new SurveysAppModule())
                    .build();
        }
        return surveysAppComponent;
    }

    public NetComponent getNetComponent() {
        if (netComponent == null) {
            netComponent = DaggerNetComponent.builder()
                    .appModule(new AppModule(this))
                    .netModule(new NetModule(Api.getHost()))
                    .build();
        }
        return netComponent;
    }

    @Override
    public AppComponent getAppComponent(Context context) {
        return getAppComponent();
    }

    @Override
    public SurveysAppComponent getSurveysAppComponent(Context context) {
        return getSurveysAppComponent();
    }

    @Override
    public NetComponent getNetComponent(Context context) {
        return getNetComponent();
    }
}
