package com.dhara.android.surveys.login.datasource;

import com.dhara.android.surveys.network.Listener;
import com.dhara.android.surveys.network.entity.Oauth;

public interface LoginDataSource {
    void getAccessToken(Listener<Oauth> listener);
}
