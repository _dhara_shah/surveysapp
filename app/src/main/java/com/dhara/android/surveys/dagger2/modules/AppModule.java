package com.dhara.android.surveys.dagger2.modules;

import android.app.Application;

import com.dhara.android.surveys.SurveysApp;
import com.dhara.android.surveys.utils.SurveysAppLog;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Singleton
@Module
public class AppModule {
    private final SurveysApp application;

    public AppModule(final SurveysApp application) {
        this.application = application;
    }

    @Provides
    Application provideApplication() {
        return application;
    }

    @Provides
    public SurveysAppLog provideSurveysAppLog() {
        return new SurveysAppLog();
    }
}
