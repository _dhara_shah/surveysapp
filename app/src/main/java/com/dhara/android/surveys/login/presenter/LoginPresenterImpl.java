package com.dhara.android.surveys.login.presenter;

import com.dhara.android.surveys.SurveysApp;
import com.dhara.android.surveys.common.ResponseListener;
import com.dhara.android.surveys.dagger2.injector.SurveysAppInjector;
import com.dhara.android.surveys.login.model.LoginInteractor;
import com.dhara.android.surveys.login.router.LoginRouter;
import com.dhara.android.surveys.login.view.LoginView;
import com.dhara.android.surveys.login.view.ViewInteractionListener;
import com.dhara.android.surveys.network.ServiceError;

import javax.inject.Inject;

public class LoginPresenterImpl implements LoginPresenter, ViewInteractionListener, ResponseListener {
    @Inject
    LoginInteractor loginInteractor;

    private LoginView view;
    private LoginRouter router;

    public LoginPresenterImpl() {
        SurveysAppInjector.from(SurveysApp.getInstance()).inject(this);
    }

    @Override
    public void handleOnCreate(final LoginView view, final LoginRouter router) {
        this.view = view;
        this.router = router;
        this.view.initViews(this);
    }

    @Override
    public void handleOnStart() {
        if (loginInteractor.isLoggedIn()) {
            // user is already signed in
            // update user's UI and lead to HomeActivity
            router.navigateToHome();
        }
    }

    @Override
    public void onLoginClicked() {
        loginInteractor.getAccessToken(this);
    }

    @Override
    public void onSuccess() {
        router.navigateToHome();
    }

    @Override
    public void onError(final ServiceError error) {
        view.showErrorMessage(error.getErrorMessage());
    }
}
