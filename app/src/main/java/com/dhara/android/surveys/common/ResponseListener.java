package com.dhara.android.surveys.common;

import com.dhara.android.surveys.network.ServiceError;

public interface ResponseListener {
    void onSuccess();

    void onError(ServiceError error);
}