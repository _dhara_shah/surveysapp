package com.dhara.android.surveys.network.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Question implements Parcelable {
    private String id;
    @SerializedName("long_text")
    private String longText;
    @SerializedName("short_text")
    private String shortText;

    protected Question(final Parcel in) {
        id = in.readString();
        longText = in.readString();
        shortText = in.readString();
    }

    public static final Creator<Question> CREATOR = new Creator<Question>() {
        @Override
        public Question createFromParcel(final Parcel in) {
            return new Question(in);
        }

        @Override
        public Question[] newArray(final int size) {
            return new Question[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getLongText() {
        return longText;
    }

    public String getShortText() {
        return shortText;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(final Parcel dest, final int flags) {
        dest.writeString(id);
        dest.writeString(longText);
        dest.writeString(shortText);
    }
}
