package com.dhara.android.surveys.home.presenter;

import com.dhara.android.surveys.home.router.HomeRouter;
import com.dhara.android.surveys.home.view.HomeView;

public interface HomePresenter {
    void handleOnCreate(HomeView view, HomeRouter router);

    void handleOnBackPress();

    boolean handleOnOptionsItemSelected(int itemId);
}
