package com.dhara.android.surveys.home.model;

import com.dhara.android.surveys.network.entity.Surveys;

import java.util.List;

public class SurveyModelAdapterImpl implements SurveyModelAdapter {
    private final List<Surveys> surveysList;

    public static SurveyModelAdapter createFrom(final List<Surveys> surveys) {
        return new SurveyModelAdapterImpl(surveys);
    }

    private SurveyModelAdapterImpl(final List<Surveys> surveysList) {
        this.surveysList = surveysList;
    }

    @Override
    public int getCount() {
        return surveysList != null ? surveysList.size() : 0;
    }

    @Override
    public String getSurveyTitle(final int position) {
        return isEmpty(surveysList) ? "" : surveysList.get(position).getTitle();
    }

    @Override
    public String getSurveyDescription(final int position) {
        return isEmpty(surveysList) ? "" : surveysList.get(position).getDescription();
    }

    @Override
    public String getCoverImage(final int position) {
        return isEmpty(surveysList) ? "" : surveysList.get(position).getCoverImageUrlLarge();
    }

    private boolean isEmpty(final List<Surveys> surveys) {
        return surveys == null || surveys.isEmpty();
    }

}
