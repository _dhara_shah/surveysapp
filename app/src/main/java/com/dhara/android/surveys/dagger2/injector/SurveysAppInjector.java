package com.dhara.android.surveys.dagger2.injector;

import android.content.Context;

import com.dhara.android.surveys.dagger2.components.SurveysAppComponent;

public class SurveysAppInjector {
    private SurveysAppInjector() {

    }

    public static SurveysAppComponent from(Context context) {
        final AppComponentProvider provider = (AppComponentProvider) context.getApplicationContext();
        return provider.getSurveysAppComponent(context);
    }
}
