package com.dhara.android.surveys.home.router;

import com.dhara.android.surveys.network.entity.Question;

import java.util.ArrayList;

public interface HomeRouter {
    void close();

    void handleOnBackPress();

    void navigateToSurvey(final ArrayList<Question> questions);
}
