package com.dhara.android.surveys.dagger2.components;

import com.dhara.android.surveys.dagger2.injection.PerActivity;
import com.dhara.android.surveys.dagger2.modules.ActivityModule;
import com.dhara.android.surveys.home.view.HomeActivity;
import com.dhara.android.surveys.login.view.LoginActivity;

import dagger.Component;

@PerActivity
@Component(modules = {ActivityModule.class}, dependencies = {AppComponent.class, SurveysAppComponent.class})
public interface ActivityComponent {
    void inject(HomeActivity homeActivity);

    void inject(LoginActivity loginActivity);
}
