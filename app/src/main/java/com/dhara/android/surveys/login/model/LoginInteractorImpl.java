package com.dhara.android.surveys.login.model;

import com.dhara.android.surveys.SurveysApp;
import com.dhara.android.surveys.common.ResponseListener;
import com.dhara.android.surveys.dagger2.injector.SurveysAppInjector;
import com.dhara.android.surveys.login.datasource.LoginDataSource;
import com.dhara.android.surveys.network.Listener;
import com.dhara.android.surveys.network.ServiceError;
import com.dhara.android.surveys.network.entity.Oauth;
import com.dhara.android.surveys.utils.ConstantsSharedPrefs;
import com.dhara.android.surveys.utils.SharedPrefHelper;

import javax.inject.Inject;

public class LoginInteractorImpl implements LoginInteractor {
    @Inject
    LoginDataSource dataSource;

    public LoginInteractorImpl() {
        SurveysAppInjector.from(SurveysApp.getInstance()).inject(this);
    }

    @Override
    public void getAccessToken(final ResponseListener listener) {
        dataSource.getAccessToken(new Listener<Oauth>() {
            @Override
            public void onSuccess(final Oauth oauthToken) {
                SharedPrefHelper.putString(ConstantsSharedPrefs.ACCESS_TOKEN, oauthToken.getAccessToken());
                listener.onSuccess();
            }

            @Override
            public void onError(final ServiceError error) {
                listener.onError(error);
            }
        });
    }

    @Override
    public boolean isLoggedIn() {
        return SharedPrefHelper.contains(ConstantsSharedPrefs.ACCESS_TOKEN);
    }
}
