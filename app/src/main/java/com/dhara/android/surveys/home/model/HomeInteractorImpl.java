package com.dhara.android.surveys.home.model;

import com.dhara.android.surveys.SurveysApp;
import com.dhara.android.surveys.common.ResponseListener;
import com.dhara.android.surveys.dagger2.injector.SurveysAppInjector;
import com.dhara.android.surveys.home.datasource.HomeDataSource;
import com.dhara.android.surveys.network.Listener;
import com.dhara.android.surveys.network.ServiceError;
import com.dhara.android.surveys.network.entity.Question;
import com.dhara.android.surveys.network.entity.Surveys;
import com.dhara.android.surveys.utils.ConstantsSharedPrefs;
import com.dhara.android.surveys.utils.SharedPrefHelper;
import com.dhara.android.surveys.utils.SurveysAppLog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import javax.inject.Inject;

public class HomeInteractorImpl implements HomeInteractor {
    @Inject
    HomeDataSource dataSource;
    @Inject
    SurveysAppLog logger;

    List<Surveys> surveyList;

    public HomeInteractorImpl() {
        SurveysAppInjector.from(SurveysApp.getInstance()).inject(this);
        surveyList = new ArrayList<>();
    }

    @Override
    public void fetchSurveys(final ResponseListener listener) {
        final Random random = new Random();
        final int page = random.nextInt(2) + 1;

        dataSource.fetchSurveys(SharedPrefHelper.getString(ConstantsSharedPrefs.ACCESS_TOKEN, ""),
                page,
                new Listener<List<Surveys>>() {
                    @Override
                    public void onSuccess(final List<Surveys> response) {
                        surveyList.clear();
                        surveyList.addAll(response);
                        listener.onSuccess();
                    }

                    @Override
                    public void onError(final ServiceError error) {
                        listener.onError(error);
                    }
                });
    }

    @Override
    public List<Surveys> getSurveyList() {
        return surveyList == null ? Collections.emptyList() : surveyList;
    }

    @Override
    public ArrayList<Question> getSurveyQuestions(final int position) {
        return getSurveyList().isEmpty() ? new ArrayList<>() : surveyList.get(position).getQuestions();
    }
}

