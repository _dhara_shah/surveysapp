package com.dhara.android.surveys.home.router;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.dhara.android.surveys.network.entity.Question;
import com.dhara.android.surveys.questions.view.TakeSurveyActivity;
import com.dhara.android.surveys.utils.ConstantIntentExtra;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class HomeRouterImpl implements HomeRouter {
    private AppCompatActivity activity;

    @Inject
    public HomeRouterImpl(final AppCompatActivity activity) {
        this.activity = activity;
    }

    @Override
    public void close() {
        activity.finish();
    }

    @Override
    public void handleOnBackPress() {
        close();
    }

    @Override
    public void navigateToSurvey(final ArrayList<Question> questions) {
        final Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(ConstantIntentExtra.EXTRA_QUESTION, questions);
        TakeSurveyActivity.startActivity(activity, bundle);
    }
}
