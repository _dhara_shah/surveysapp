package com.dhara.android.surveys.login.view;

import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import com.dhara.android.surveys.R;

import javax.inject.Inject;

public class LoginViewImpl implements LoginView {
    private AppCompatActivity activity;
    private Button btnLogin;

    @Inject
    public LoginViewImpl(AppCompatActivity activity) {
        this.activity = activity;
    }

    @Override
    public void initViews(ViewInteractionListener listener) {
        btnLogin = activity.findViewById(R.id.btn_login);

        btnLogin.setOnClickListener(v -> {
                listener.onLoginClicked();
        });
    }

    @Override
    public void showErrorMessage(final String errorMessage) {
        Snackbar.make(btnLogin, errorMessage, Snackbar.LENGTH_LONG).show();
    }
}
