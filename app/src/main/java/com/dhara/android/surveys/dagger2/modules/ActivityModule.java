package com.dhara.android.surveys.dagger2.modules;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;

import com.dhara.android.surveys.dagger2.injection.PerActivity;
import com.dhara.android.surveys.dagger2.scope.ActivityScope;
import com.dhara.android.surveys.home.presenter.HomePresenter;
import com.dhara.android.surveys.home.presenter.HomePresenterImpl;
import com.dhara.android.surveys.home.router.HomeRouter;
import com.dhara.android.surveys.home.router.HomeRouterImpl;
import com.dhara.android.surveys.home.view.HomeView;
import com.dhara.android.surveys.home.view.HomeViewImpl;
import com.dhara.android.surveys.login.presenter.LoginPresenter;
import com.dhara.android.surveys.login.presenter.LoginPresenterImpl;
import com.dhara.android.surveys.login.router.LoginRouter;
import com.dhara.android.surveys.login.router.LoginRouterImpl;
import com.dhara.android.surveys.login.view.LoginView;
import com.dhara.android.surveys.login.view.LoginViewImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {
    private AppCompatActivity activity;

    public ActivityModule(AppCompatActivity activity) {
        this.activity = activity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return activity;
    }

    @Provides
    @ActivityScope
    Context providesContext() {
        return activity;
    }

    @Provides
    @PerActivity
    public HomePresenter provideHomePresenter(){
        return new HomePresenterImpl();
    }

    @Provides
    @PerActivity public HomeView provideHomeView() {
        return new HomeViewImpl(activity);
    }

    @Provides
    @PerActivity public HomeRouter provideHomeRouter() {
        return new HomeRouterImpl(activity);
    }

    @Provides
    @PerActivity
    public LoginPresenter provideLoginPresenter(){
        return new LoginPresenterImpl();
    }

    @Provides
    @PerActivity public LoginView provideLoginsView() {
        return new LoginViewImpl(activity);
    }

    @Provides
    @PerActivity public LoginRouter provideLoginRouter() {
        return new LoginRouterImpl(activity);
    }
}