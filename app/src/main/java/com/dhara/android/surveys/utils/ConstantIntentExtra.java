package com.dhara.android.surveys.utils;

public abstract class ConstantIntentExtra {
    private static final String PACKAGE_NAME = ConstantIntentExtra.class.getPackage().getName();
    public static final String EXTRA_QUESTION = PACKAGE_NAME + "_EXTRA_QUESTION";
}
