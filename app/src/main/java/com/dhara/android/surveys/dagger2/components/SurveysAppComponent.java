package com.dhara.android.surveys.dagger2.components;

import com.dhara.android.surveys.dagger2.modules.AppModule;
import com.dhara.android.surveys.dagger2.modules.SurveysAppModule;
import com.dhara.android.surveys.home.model.HomeInteractorImpl;
import com.dhara.android.surveys.home.presenter.HomePresenterImpl;
import com.dhara.android.surveys.login.model.LoginInteractorImpl;
import com.dhara.android.surveys.login.presenter.LoginPresenterImpl;

import dagger.Component;

@Component(modules = {SurveysAppModule.class, AppModule.class})
public interface SurveysAppComponent {
    void inject(HomePresenterImpl presenter);

    void inject(HomeInteractorImpl mainInteractor);

    void inject(LoginPresenterImpl loginPresenter);

    void inject(LoginInteractorImpl loginInteractor);
}
