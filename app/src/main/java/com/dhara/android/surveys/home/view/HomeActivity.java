package com.dhara.android.surveys.home.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.dhara.android.surveys.BaseActivity;
import com.dhara.android.surveys.R;
import com.dhara.android.surveys.home.presenter.HomePresenter;
import com.dhara.android.surveys.home.router.HomeRouter;

import javax.inject.Inject;

public class HomeActivity extends BaseActivity {

    @Inject
    HomePresenter presenter;

    @Inject
    HomeRouter router;

    @Inject
    HomeView homeView;

    public static void startActivity(final Context context) {
        final Intent intent = new Intent(context, HomeActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
        setContentView(R.layout.activity_home);
        presenter.handleOnCreate(homeView, router);
    }

    @Override
    public void onBackPressed() {
        presenter.handleOnBackPress();
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(final MenuItem item) {
        return presenter.handleOnOptionsItemSelected(item.getItemId());
    }
}
