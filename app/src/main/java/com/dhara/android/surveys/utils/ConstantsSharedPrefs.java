package com.dhara.android.surveys.utils;

public interface ConstantsSharedPrefs {
    String SHARED_PREFERENCES = "surveysapp_prefs";
    String ACCESS_TOKEN = "access_token";
}
