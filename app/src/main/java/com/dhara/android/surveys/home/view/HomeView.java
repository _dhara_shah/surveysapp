package com.dhara.android.surveys.home.view;

import com.dhara.android.surveys.home.model.SurveyModelAdapter;

public interface HomeView {
    void initViews(final SurveyModelAdapter modelAdapter, ViewInteractionListener listener);

    void showProgress();

    void hideProgress();

    void showError(String errorMessage);

    void updateData(final SurveyModelAdapter modelAdapter);

    boolean handleOnOptionsItemSelected(int itemId);

    boolean closeDrawerLayout();
}
