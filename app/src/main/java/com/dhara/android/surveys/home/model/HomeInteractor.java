package com.dhara.android.surveys.home.model;

import com.dhara.android.surveys.common.ResponseListener;
import com.dhara.android.surveys.network.entity.Question;
import com.dhara.android.surveys.network.entity.Surveys;

import java.util.ArrayList;
import java.util.List;

public interface HomeInteractor {
    void fetchSurveys(ResponseListener listener);

    List<Surveys> getSurveyList();

    ArrayList<Question> getSurveyQuestions(int position);
}
