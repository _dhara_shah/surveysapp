package com.dhara.android.surveys.home.view;

import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;

import com.dhara.android.surveys.R;
import com.dhara.android.surveys.home.model.SurveyModelAdapter;
import com.viewpagerindicator.CirclePageIndicator;

import javax.inject.Inject;

public class HomeViewImpl implements HomeView, NavigationView.OnNavigationItemSelectedListener {
    private AppCompatActivity activity;
    private View progressView;
    private CirclePageIndicator circularIndicator;
    private DrawerLayout drawer;
    private ViewInteractionListener listener;
    private SurveyPagerAdapter pagerAdapter;
    private ViewPager viewPager;

    @Inject
    public HomeViewImpl(final AppCompatActivity activity) {
        this.activity = activity;
    }

    @Override
    public void initViews(final SurveyModelAdapter modelAdapter, final ViewInteractionListener listener) {
        this.listener = listener;
        setUpToolbar();

        progressView = activity.findViewById(R.id.lnr_progress_view);
        circularIndicator = activity.findViewById(R.id.circle_page_indicator);

        final NavigationView navigationView = activity.findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        pagerAdapter = new SurveyPagerAdapter(modelAdapter, listener);

        viewPager = activity.findViewById(R.id.view_pager);
        viewPager.setAdapter(pagerAdapter);
        circularIndicator.setViewPager(viewPager);
    }

    @Override
    public void showProgress() {
        progressView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressView.setVisibility(View.GONE);
    }

    @Override
    public void showError(final String errorMessage) {
        Snackbar.make(drawer, errorMessage, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void updateData(final SurveyModelAdapter modelAdapter) {
        final SurveyPagerAdapter pagerAdapter = new SurveyPagerAdapter(modelAdapter, listener);
        viewPager.setAdapter(pagerAdapter);
        circularIndicator.setViewPager(viewPager);
    }

    @Override
    public boolean handleOnOptionsItemSelected(final int itemId) {
        if (itemId == R.id.action_refresh) {
            listener.onRefreshClicked();
            return true;
        }
        return false;
    }

    @Override
    public boolean closeDrawerLayout() {
        if (drawer.isDrawerOpen(Gravity.START)) {
            drawer.closeDrawer(Gravity.START);
            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull final MenuItem item) {
        // Handle navigation view item clicks here.
        final int id = item.getItemId();

        if (id == R.id.nav_profile) {

        } else if (id == R.id.nav_signout) {

        } else if (id == R.id.nav_share) {

        }

        drawer = activity.findViewById(R.id.drawer_layout);
        drawer.closeDrawer(Gravity.START);
        return true;
    }

    private void setUpToolbar() {
        final Toolbar toolbar = activity.findViewById(R.id.toolbar);
        activity.setSupportActionBar(toolbar);

        drawer = activity.findViewById(R.id.drawer_layout);
        final ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                activity, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        toolbar.setNavigationOnClickListener(v -> {
            if (drawer.isDrawerOpen(Gravity.START)) {
                drawer.closeDrawer(Gravity.START);
            } else {
                drawer.openDrawer(Gravity.START);
            }
        });
    }
}
