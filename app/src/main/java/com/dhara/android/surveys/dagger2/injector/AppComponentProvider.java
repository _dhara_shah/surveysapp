package com.dhara.android.surveys.dagger2.injector;

import android.content.Context;

import com.dhara.android.surveys.dagger2.components.AppComponent;
import com.dhara.android.surveys.dagger2.components.NetComponent;
import com.dhara.android.surveys.dagger2.components.SurveysAppComponent;

public interface AppComponentProvider {
    AppComponent getAppComponent(Context context);

    SurveysAppComponent getSurveysAppComponent(Context context);

    NetComponent getNetComponent(Context context);
}
