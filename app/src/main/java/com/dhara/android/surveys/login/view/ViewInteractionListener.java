package com.dhara.android.surveys.login.view;

public interface ViewInteractionListener {
    void onLoginClicked();
}
