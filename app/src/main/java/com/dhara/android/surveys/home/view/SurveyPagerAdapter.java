package com.dhara.android.surveys.home.view;

import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dhara.android.surveys.R;
import com.dhara.android.surveys.home.model.SurveyModelAdapter;

public class SurveyPagerAdapter extends PagerAdapter {
    private final SurveyModelAdapter modelAdapter;
    private final ViewInteractionListener listener;

    public SurveyPagerAdapter(final SurveyModelAdapter modelAdapter, final ViewInteractionListener listener) {
        this.modelAdapter = modelAdapter;
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return modelAdapter.getCount();
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        final LayoutInflater inflater = LayoutInflater.from(container.getContext());
        final ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.item_survey_card_individual,
                container, false);

        bindView(layout, position);

        container.addView(layout);
        return layout;
    }

    @Override
    public boolean isViewFromObject(@NonNull final View view, @NonNull final Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull final ViewGroup container, final int position, @NonNull final Object view) {
        container.removeView((View) view);
    }

    private void bindView(final ViewGroup layout, final int position) {
        final TextView txtTitle = layout.findViewById(R.id.txt_survey_name);
        txtTitle.setText(modelAdapter.getSurveyTitle(position));

        final ImageView imgCover = layout.findViewById(R.id.img_cover);
        Glide.with(layout.getContext())
                .load(modelAdapter.getCoverImage(position))
                .into(imgCover);

        final TextView txtDesc = layout.findViewById(R.id.txt_description);
        txtDesc.setText(modelAdapter.getSurveyDescription(position));

        final Button btnTakeSurvey = layout.findViewById(R.id.btn_take_survey);
        btnTakeSurvey.setOnClickListener(v -> listener.onTakeSurveysClicked(position));
    }
}
