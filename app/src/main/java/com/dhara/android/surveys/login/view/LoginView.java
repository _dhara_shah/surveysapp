package com.dhara.android.surveys.login.view;

public interface LoginView {
    void initViews(ViewInteractionListener listener);

    void showErrorMessage(String errorMessage);
}
