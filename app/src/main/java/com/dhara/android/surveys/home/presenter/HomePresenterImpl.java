package com.dhara.android.surveys.home.presenter;

import com.dhara.android.surveys.SurveysApp;
import com.dhara.android.surveys.common.ResponseListener;
import com.dhara.android.surveys.dagger2.injector.SurveysAppInjector;
import com.dhara.android.surveys.home.model.HomeInteractor;
import com.dhara.android.surveys.home.model.SurveyModelAdapterImpl;
import com.dhara.android.surveys.home.router.HomeRouter;
import com.dhara.android.surveys.home.view.HomeView;
import com.dhara.android.surveys.home.view.ViewInteractionListener;
import com.dhara.android.surveys.network.ServiceError;

import javax.inject.Inject;

public class HomePresenterImpl implements HomePresenter, ViewInteractionListener, ResponseListener {
    private HomeView view;
    private HomeRouter router;

    @Inject
    HomeInteractor interactor;

    @Inject
    public HomePresenterImpl() {
        SurveysAppInjector.from(SurveysApp.getInstance()).inject(this);
    }

    @Override
    public void handleOnCreate(final HomeView view, final HomeRouter router) {
        this.view = view;
        this.router = router;
        view.initViews(SurveyModelAdapterImpl.createFrom(interactor.getSurveyList()), this);
        view.showProgress();
        interactor.fetchSurveys(this);
    }

    @Override
    public void handleOnBackPress() {
        if (view.closeDrawerLayout()) {
            router.handleOnBackPress();
        }
    }

    @Override
    public boolean handleOnOptionsItemSelected(final int itemId) {
        return view.handleOnOptionsItemSelected(itemId);
    }

    @Override
    public void onSuccess() {
        view.hideProgress();
        view.updateData(SurveyModelAdapterImpl.createFrom(interactor.getSurveyList()));
    }

    @Override
    public void onError(final ServiceError error) {
        view.hideProgress();
        view.showError(error.getErrorMessage());
    }

    @Override
    public void onRefreshClicked() {
        view.showProgress();
        interactor.fetchSurveys(this);
    }

    @Override
    public void onTakeSurveysClicked(final int position) {
        router.navigateToSurvey(interactor.getSurveyQuestions(position));
    }
}
