package com.dhara.android.surveys.questions.view;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.dhara.android.surveys.BaseActivity;
import com.dhara.android.surveys.R;

public class TakeSurveyActivity extends BaseActivity {
    public static void startActivity(Context context, Bundle bundle) {
        Intent intent = new Intent(context, TakeSurveyActivity.class);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_survey);
    }
}
