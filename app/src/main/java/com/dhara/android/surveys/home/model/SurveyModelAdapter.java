package com.dhara.android.surveys.home.model;

public interface SurveyModelAdapter {
    int getCount();

    String getSurveyTitle(int position);

    String getSurveyDescription(int position);

    String getCoverImage(int position);
}
