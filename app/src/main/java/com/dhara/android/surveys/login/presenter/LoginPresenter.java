package com.dhara.android.surveys.login.presenter;

import com.dhara.android.surveys.login.router.LoginRouter;
import com.dhara.android.surveys.login.view.LoginView;

public interface LoginPresenter {
    void handleOnCreate(LoginView view, LoginRouter router);

    void handleOnStart();
}
