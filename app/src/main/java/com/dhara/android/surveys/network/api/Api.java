package com.dhara.android.surveys.network.api;

public class Api {
    private static final String BASE_URL = "https://nimbl3-survey-api.herokuapp.com/";
    private static final String GRANT_TYPE = "password";
    private static final String USER_NAME = "carlos@nimbl3.com";
    private static final String PASSWORD = "antikera";

    private Api() { }

    public static String getHost() {
        return BASE_URL;
    }

    public static String getGrantType() {
        return GRANT_TYPE;
    }

    public static String getUserName() {
        return USER_NAME;
    }

    public static String getPassword() {
        return PASSWORD;
    }
}
