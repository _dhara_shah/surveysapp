package com.dhara.android.surveys.login.model;

import com.dhara.android.surveys.common.ResponseListener;

public interface LoginInteractor {
    void getAccessToken(ResponseListener listener);

    boolean isLoggedIn();
}
