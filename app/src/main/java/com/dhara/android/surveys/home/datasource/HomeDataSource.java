package com.dhara.android.surveys.home.datasource;

import com.dhara.android.surveys.network.Listener;
import com.dhara.android.surveys.network.entity.Surveys;

import java.util.List;

public interface HomeDataSource {
    void fetchSurveys(String accessToken, int page, Listener<List<Surveys>> listener);
}
