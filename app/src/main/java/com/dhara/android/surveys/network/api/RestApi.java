package com.dhara.android.surveys.network.api;

import com.dhara.android.surveys.network.entity.Oauth;
import com.dhara.android.surveys.network.entity.Surveys;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface RestApi {
    @FormUrlEncoded
    @POST("/oauth/token")
    Call<Oauth> getAccessToken(@Field(value = "grant_type", encoded = true) String grantType,
                               @Field(value = "username", encoded = true) String userName,
                               @Field(value = "password", encoded = true) String password);

    @GET("/surveys.json?per_page=10")
    Observable<List<Surveys>> getSurveys(@Query(value = "access_token", encoded = true) String accessToken,
                                         @Query(value = "page") int page);
}