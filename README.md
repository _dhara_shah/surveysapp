# README #

### What is this repository for? ###

* SurveysApp
* Version : 1.0

### What the app does ###
* The application list a set of different surveys a user can take.
* It can be paginated (but for now it is not paginated)
* I have used the page attribute to reflect refresh when the refresh icon is clicked;. 

Ideally the refresh icon should get new data and add it to the current container, and display it, but
in order to reflect that reflect is sort of in function, I have used the page attribute, and randomly, 
when refresh is tapped on, either page 1 will get loaded or page 2 will get loaded.

### How do I get set up? ###

* Need the latest android studio with Java 1.8 support (for lambda support)
* The rest should be the same like every other application.

### Dependencies
1. Mockito and PowerMockito for testing
2. Android support libraries
3. Glide for image loading
5. Dagger2 for dependency injections
6. Retrofit with RxJava

### Deployment instructions
1. Run the project just like every other project and you should be done.

### Contribution guidelines ###

* MVP with Interactors and Router has been used as the architectural design
* Follow android and java coding standards

### Who do I talk to? ###

* Dhara Shah
* Contact: sdhara2@hotmail.com